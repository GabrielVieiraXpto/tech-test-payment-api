using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;


using Desafio.Context;

namespace Desafio.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController  : ControllerBase
    {
        private readonly Seguradora _context;
        public VendaController(Seguradora context)
        {
            _context = context;
        } 

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if(venda.Itens.Count == 0)
            {
                return BadRequest();
            }
            
            venda.Status = EnumStatusVenda.Aguadando_Pagamento;
            _context.Vendas_Db.Add(venda);
            _context.SaveChanges();  
            return CreatedAtAction(nameof(ObterPorID), new { id = venda.Id }, venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorID(int id)
        { 
            var venda = _context.Vendas_Db.Include(x => x.Itens).FirstOrDefault(x => x.Id == id);
            venda = _context.Vendas_Db.Include(x =>x.Vendedor).FirstOrDefault(x => x.Id == id);

            if (venda == null)
            {
                return NotFound();
            }
             
            return Ok(venda);
        }
       
        [HttpPatch("{id}")]
        public IActionResult AtualizarStatus(EnumStatusVenda statusVenda,int id  )
        {
            var vendaAtualizada = _context.Vendas_Db.Find(id);
            if (vendaAtualizada == null)
            {
                return NotFound("Nenhuma venda encontrada");
            }
            else
            {
                vendaAtualizada = _context.Vendas_Db.Include(x => x.Itens).FirstOrDefault(x => x.Id == id);
                vendaAtualizada = _context.Vendas_Db.Include(x =>x.Vendedor).FirstOrDefault(x => x.Id == id);
            }
            if(vendaAtualizada.Status == EnumStatusVenda.Aguadando_Pagamento&& statusVenda == EnumStatusVenda.Pagamento_Aprovado)
            {
               vendaAtualizada.Status = statusVenda;
               _context.SaveChanges();
               return Ok(vendaAtualizada);
       
            }
            else if(vendaAtualizada.Status == EnumStatusVenda.Pagamento_Aprovado && statusVenda == EnumStatusVenda.Enviado_Para_Transportadora)
            {
                vendaAtualizada.Status = statusVenda;
                _context.SaveChanges();
                return Ok(vendaAtualizada);
            }
            else if(vendaAtualizada.Status == EnumStatusVenda.Enviado_Para_Transportadora && statusVenda == EnumStatusVenda.Entregue)
            {
                vendaAtualizada.Status = statusVenda;
                _context.SaveChanges();
                return Ok(vendaAtualizada);
            }
            else if(vendaAtualizada.Status==EnumStatusVenda.Aguadando_Pagamento||vendaAtualizada.Status==EnumStatusVenda.Pagamento_Aprovado&&statusVenda==EnumStatusVenda.Cancelada)
            {
                vendaAtualizada.Status = statusVenda;
                _context.SaveChanges();
                return Ok(vendaAtualizada);
            }
            else
            {
                return BadRequest($"Transição de status inválida \n Status atual: {vendaAtualizada.Status} \n Status desejado: {statusVenda}");
            }

        }    
    }
}