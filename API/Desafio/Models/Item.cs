using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;

namespace Desafio.Models
{
    public class Item_Venda
    {
        public int Id { get; set; }
        public string Nome { get; set; }= null!;
        public decimal Valor { get; set; }  
    }
}