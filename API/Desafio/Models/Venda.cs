using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desafio.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public EnumStatusVenda Status { get; set; }
        public DateTime Data { get; set; }
        public Vendedor_Venda Vendedor { get; set; }=new Vendedor_Venda();
        public List<Item_Venda> Itens { get; set; }= new List<Item_Venda>();        
    }
}