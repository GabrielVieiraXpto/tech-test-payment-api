using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;

namespace Desafio.Models
{
    public class Vendedor_Venda
    {
        public int Id { get; set; }
        public string CPF { get; set; }= null!;
        public string Nome { get; set; }= null!;
        public string Email { get; set; }= null!;
        public string Telefone { get; set; }= null!;
    }
}